<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/foo/bar', function() {

    return view('foo.bar', ['foo' => 'Var 1', 'bar' => 'var 2']);
});

Route::post('fooPost', function() {
return "Olá Post";
});

Route::get('/foo/baz', function() {

    return view('foo.baz');
});

Route::group(['prefix' => 'user'], function() {

    Route::get('/', ['uses' => 'UserController@index']);
    Route::get('add', ['uses' => 'UserController@create']);
    Route::post('add', ['uses' => 'UserController@post']);
    Route::get('{id}', ['uses' => 'UserController@show']);
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/client', 'ClientController@index');
Route::post('/client', 'ClientController@store');
Route::get('/client/{id}', 'ClientController@show');
Route::delete('/client/{id}', 'ClientController@destroy');
Route::put('client/{id}', 'ClientController@update');


Route::get('/project/{id}/note', 'ProjectNoteController@index');
Route::post('/project/{id}/note', 'ProjectNoteController@store');
Route::get('/project/{id}/note/{noteId}', 'ProjectNoteController@show');
Route::put('/project/{id}/note/{noteId}', 'ProjectNoteController@update');
Route::delete('/project/{id}/note/{noteId}', 'ProjectNoteController@destroy');

Route::get('/project', 'ProjectController@index');
Route::post('/project', 'ProjectController@store');
Route::get('/project/{id}', 'ProjectController@show');
Route::put('/project/{id}', 'ProjectController@update');
Route::delete('/project/{id}', 'ProjectController@destroy');


/* Auth TESTES*/
Route::get('/redirect', function () {

    $query = http_build_query([
        'client_id' => '3',
        'redirect_uri' => 'http://laravel.dev/callback',
        'response_type' => 'code',
        'scope' => ''
    ]);

    return redirect('http://laravel.dev/oauth/authorize?'.$query);
});

Route::get('/callback', function (Illuminate\Http\Request $request) {
    $http = new \GuzzleHttp\Client;

    $response = $http->post('http://laravel.dev/oauth/token', [
        'form_params' => [
            'client_id' => '3',
            'client_secret' => 'bAcjAeEc5yr63Iz9PjPcctxA2mGlxjYtFq9AiA09',
            'grant_type' => 'authorization_code',
            'redirect_uri' => 'http://laravel.dev/callback',
            'code' => $request->code,
        ],
    ]);
    return json_decode((string) $response->getBody(), true);
});

