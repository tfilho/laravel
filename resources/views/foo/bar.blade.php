@extends('foo.base')

@section('title','Bar page')

@section('container')
    <h2>Bar</h2>
    <p> parágrafo</p>
    
    @if (true)
        return true<br /><hr />
    @endif
    
    <ul>
    @foreach(['foo','bar','baz'] as $item)
    
        <li>{{$item}}</li>
    @endforeach
    </ul>
    
@endsection