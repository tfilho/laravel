<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use App\Entities\Client;
use Prettus\Repository\Eloquent\BaseRepository;
/**
 * Description of ClientRepositoryEloquent
 *
 * @author amd
 */
class ClientRepositoryEloquent extends BaseRepository implements ClientRepository
{
    public function model() {
        
        return Client::class;
    }
}
