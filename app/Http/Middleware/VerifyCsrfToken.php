<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    
    //Pra funcionar POST, DELETE, PUT ou ANY no Postman a URI deve ser colocada em exceção em: app\Http\Middleware\VerifyCsrfToken.php
    protected $except = [
        'fooPost',
    ];
}