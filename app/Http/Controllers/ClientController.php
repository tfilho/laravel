<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\ClientRepository;
use App\Services\ClientService;

class ClientController extends Controller {

    /**
     * @var App\Services\ClientService
     */
    private $service;
    private $repository;

    public function __construct(ClientRepository $repository, ClientService $service) {

        $this->repository = $repository;
        $this->service = $service;
    }

    public function index() {

        return $this->repository->all();
    }

    public function show($id) {

        try {
            return $this->repository->find($id);
            //return ['success' => true, 'Projeto encontrado com sucesso!'];
        } catch (QueryException $e) {
            return ['error' => true, 'Cliente não pode ser encontrado'];
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'Cliente não encontrado.'];
        } catch (Exception $e) {
            return ['error' => true, 'Ocorreu algum erro ao encontrar o cliente.'];
        }
    }

    public function store(Request $request) {

        //dd($request->all());
        return $this->service->create($request->all());
        //return Client::create($request->all());
    }

    public function destroy($id) {

        try {
            $this->repository->find($id)->delete();
            return ['success' => true, 'Cliente deletado com sucesso!'];
        } catch (QueryException $e) {
            return ['error' => true, 'Cliente não pode ser apagado pois existe um ou mais projetos vinculados a ele.'];
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'Cliente não encontrado.'];
        } catch (Exception $e) {
            return ['error' => true, 'Ocorreu algum erro ao excluir o cliente.'];
        }
    }

    public function update(Request $request, $id) {

        try {
            return $this->service->update($request->all(), $id);
            //return $this->show($id);
            //return ['success' => true, "Projeto ID= {$id} atualizado com sucesso!"];
        } catch (QueryException $e) {
            return ['error' => true, 'Cliente não pode ser atualizado'];
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'Cliente não encontrado.'];
        } catch (Exception $e) {
            return ['error' => true, 'Ocorreu algum erro ao atualizar o cliente.'];
        }
    }

}
