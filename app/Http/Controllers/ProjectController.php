<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\ProjectRepository;
use App\Services\ProjectService;

class ProjectController extends Controller {

    /**
     * @var App\Services\ClientService
     */
    private $service;
    private $repository;

    public function __construct(ProjectRepository $repository, ProjectService $service) {

        $this->repository = $repository;
        $this->service = $service;
    }

    public function index() {

        return $this->repository->all();
    }

    public function show($id) {

        //return $this->repository->find($id);
        try {
            $this->repository->find($id);
            return ['success' => true, 'Projeto encontrado com sucesso!'];
        } catch (QueryException $e) {
            return ['error' => true, 'Projeto não pode ser encontrado'];
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'Projeto não encontrado.'];
        } catch (Exception $e) {
            return ['error' => true, 'Ocorreu algum erro ao encontrar o projeto.'];
        }
    }

    public function store(Request $request) {

        //dd($request->all());
        return $this->service->create($request->all());
        //return Client::create($request->all());
    }

    public function destroy($id) {
        
        try {
            $this->repository->find($id)->delete();
            //$this->repository->findOrFail($id)->delete();
            return ['success' => true, 'Projeto deletado com sucesso!'];
        } catch (QueryException $e) {
            return ['error' => true, 'Projeto não pode ser apagado pois existe um ou mais clientes vinculados a ele.'];
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'Projeto não encontrado.'];
        } catch (Exception $e) {
            return ['error' => true, 'Ocorreu algum erro ao excluir o projeto.'];
        }
    }

    public function update(Request $request, $id) {

        //Client::find($id)->update($request->all());
        
        try {
            return $this->service->update($request->all(), $id);
            //return $this->show($id);
            //return ['success' => true, "Projeto ID= {$id} atualizado com sucesso!"];
        } catch (QueryException $e) {
            return ['error' => true, 'Projeto não pode ser atualizado'];
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'Projeto não encontrado.'];
        } catch (Exception $e) {
            return ['error' => true, 'Ocorreu algum erro ao atualizar o projeto.'];
        }
    }

}
