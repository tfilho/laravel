<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProjectNoteRepository;
use App\Services\ProjectNoteService;

class ProjectNoteController extends Controller {

    /**
     * @var App\Services\ClientService
     */
    private $service;
    private $repository;

    public function __construct(ProjectNoteRepository $repository, ProjectNoteService $service) {

        $this->repository = $repository;
        $this->service = $service;
    }

    public function index($id) {

        return $this->repository->findWhere(['project_id' => $id]);
    }

    public function show($id, $noteId) {

        return $this->repository->findWhere(['project_id' => $id, 'id' => $noteId]);
    }

    public function store(Request $request) {

        //dd($request->all());
        return $this->service->create($request->all());
        //return Client::create($request->all());
    }

    public function destroy($id, $noteId) {

        //Client::find($id)->delete();
        $this->repository->delete($noteId);
    }

    public function update(Request $request, $id, $noteId) {

        //Client::find($id)->update($request->all());
        $this->service->update($request->all(), $noteId);
        return $this->show($id);
    }

}
