<?php

namespace App\Services;

use \App\Repositories\ClientRepository;
use \App\Validators\ClientValidator;
use \Prettus\Validator\Exceptions\ValidatorException;

class ClientService {

    /**
     * @var ClientValidator
     */
    private $validator;
    protected $repository;

    public function __construct(ClientRepository $repository, ClientValidator $validator) {

        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function create(array $data) {

        //enviar email
        //disparar notificação
        //postar tweet

        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $exc) {
            return [
                'error' => 'true',
                'message' => $exc->getMessageBag()
            ];
        }
    }

    public function update(array $data, $id) {

        
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $exc) {
            return [
                'error' => 'true',
                'message' => $exc->getMessageBag()
            ];
        }
    }

}
