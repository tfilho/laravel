<?php

namespace App\Services;

use \App\Repositories\ProjectRepository;
use \App\Validators\ProjectValidator;
use \Prettus\Validator\Exceptions\ValidatorException;

class ProjectService {

    /**
     * @var ClientValidator
     */
    private $validator;
    protected $repository;

    public function __construct(ProjectRepository $repository, ProjectValidator $validator) {

        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function create(array $data) {

        //enviar email
        //disparar notificação
        //postar tweet

        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $exc) {
            return [
                'error' => 'true',
                'message' => $exc->getMessageBag()
            ];
        }
    }

    public function update(array $data, $id) {

        
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->find($id)->update($data, $id);
        } catch (ValidatorException $exc) {
            return [
                'error' => 'true',
                'message' => $exc->getMessageBag()
            ];
        }
    }

}
